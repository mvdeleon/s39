/*

App: Booking System API

Course Booking System (Web App)


-User login
-user registration

Customer/authenticated Users:
-View course (all active courses)
-enroll course

admin users:
-add course
-update course
-archive/unarchive a course (soft delete/reactivate the course)
-view courses (all courses active/inactive)
-view/manage user accounts**

all users(guests, customers, admin)
-view active courses


Data Model for the booking system

Two-way Embedding

user{
	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments:[{

		id - document identifier
		courseId - the unique identifier
		courseName: -optional
		isPaid,
		dateEnrolled

	}]
}

course {
	
	id - unique for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id - document identifier
			userId,
			isPaid,
			dateEnrolled

		}


	]

}

*/

/* WITH REFERENCING

user{
	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments:[{

		id - document identifier
		courseId - the unique identifier
		courseName: -optional
		isPaid,
		dateEnrolled

	}]
}

course {
	
	id - unique for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,

}

enrollments [

		id - document identifier
		userId - unique identifier for user
		courseId - the unique identifier
		courseName: -optional
		isPaid,
		dateEnrolled

	}

*/

/*
	1. Create models folder and create Course.js - schema
	2. Put the data



*/
