const express = require("express");
const router = express.Router();


const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// CREATE COURSE


router.post("/",auth.verify,(req,res)=>{

	const data={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseControllers.addCourse(data.course,data.isAdmin).then(resultFromController=>res.send(resultFromController));



});
	








module.exports = router;